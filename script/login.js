function login() {
    var user = $("#login-form").serializeObject();
    getUser(user.email, user.password);
}

function getUser(email, password) {
    $.ajax({
        url: "http://localhost:3000/users",
        data: { email: email, password: password },
        type: "GET",
        dataType: "json",
        success: function (data) {
            if (data.length < 1) {
                showPopup("Error", "user does not exitst");
            } else if (data.length == 1) {
                var user = data[0];
                sessionStorage.user = JSON.stringify(user);
                window.location = "index.html";
            }
        }, error: function (data) {
            showPopup("Error", "there was a system error");
        }
    });
}