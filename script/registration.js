
function register() {
    var user = $("#reg-form").serializeObject();
    if (validateForm(user)) {
        createUser(user);
    }
}

function validateForm(user) {
    if (!validatePhone(user.phone)) {
        showPopup("Error", "invalid phone");
        return false;
    } else if (!validateId(user.idnumber)) {
        showPopup("Error", "invalid id");
        return false;
    } else if (user.email == null || !validateEmail(user.email)) {
        showPopup("Error", "invalid email");
        return false;
    } else if (!validatePassword(user.password, $("#repassword").val())) {
        showPopup("Error", "please ensure password is typeed correctly");
        return false;
    }
    return true;
}

function validatePhone(phoneNumber) {
    var phonePattern = /^\d{10}$/;
    return phoneNumber.match(phonePattern);
}

function validateId(idNumber) {
    var idPattern = /^\d{13}$/;
    return idNumber.match(idPattern);
}

function validateEmail(email) {
    var emailPattern = /^[\w\.]+@\w+\.\w+$/;
    return email.match(emailPattern);
}

function validatePassword(password, repassword) {
    console.log(password);
    console.log(repassword);
    return password == repassword;
}

function createUser(user) {
    $.ajax({
        url: "http://localhost:3000/users",
        data: user,
        type: "POST",
        dataType: "json",
        success: function (data) {
            showPopup("Success", "user was created");
        }, error: function () {
            showPopup("Error", "user was not created");
        }
    });
}
