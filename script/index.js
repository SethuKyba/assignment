var userText = sessionStorage.user;
var user = (userText!=null) ? JSON.parse(userText) :  null;

$(document).ready(function () {
    if (user != null) {
        $("#welcome-text").text("Welcome " + user.firstname + " " + user.lastname);
    } else {
        showPopup("Error", "You are not logged in");
    }
});

function logout()   {
    sessionStorage.clear();
    window.location = "login.html";
}
